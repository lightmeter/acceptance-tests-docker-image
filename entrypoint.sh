#!/bin/sh

set -e -o pipefail

xvfb-run -s "-screen 0 $TAIKO_SCREEN_RES" "$@"
