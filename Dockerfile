FROM node:14-alpine

USER root

ENV IMAGE_DATE 20201210

RUN apk add xvfb-run chromium coreutils python3 pixman-dev cairo-dev pango-dev make g++ jpeg-dev git

ADD entrypoint.sh /entrypoint.sh

USER node

ENV TAIKO_SKIP_CHROMIUM_DOWNLOAD 1

ENV NPM_CONFIG_PREFIX=/home/node/.npm-global

ENV PATH="/home/node/node_modules/.bin:/home/node/.npm-global/bin/:${PATH}"

RUN npm install -g taiko '@getgauge/cli' && gauge install html-report xml-report js && npm cache clear --force

ENV TAIKO_BROWSER_PATH /usr/bin/chromium-browser

ENV TAIKO_SCREEN_RES 1920x1080x16

ENV NODE_PATH /home/node/node_modules

ENTRYPOINT ["/entrypoint.sh"]
